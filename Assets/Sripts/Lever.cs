﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour {

    private GameObject ActivePlayer;
    public GameObject RotateTutorial;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (ActivePlayer && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.E)))
        {
            Debug.Log("ROTATING CUBE");
            Cube cube = transform.parent.GetComponentInParent<Cube>();
            if (cube)
            {
                cube.RotateCube();
            }
        }		
	}

    private void OnTriggerEnter(Collider other)
    {
        // Change the cube color to green.
        if (other.gameObject.CompareTag("Player"))
        {
            MeshRenderer meshRend = GetComponent<MeshRenderer>();
            meshRend.material.color = Color.green;
            ActivePlayer = other.gameObject;
            if (RotateTutorial)
            {
                RotateTutorial.gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            // Revert the cube color to white.
            MeshRenderer meshRend = GetComponent<MeshRenderer>();
            meshRend.material.color = Color.white;
            ActivePlayer = null;
            if (RotateTutorial)
            {
                RotateTutorial.gameObject.SetActive(false);
            }
        }
    }
}
