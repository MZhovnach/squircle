﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesCollider : MonoBehaviour {

    public Cube myCube;
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Cube")
        {
            Debug.Log(gameObject.name + "is connected to " + collision.gameObject.GetComponent<CubesCollider>().myCube.name);
            myCube.attachingCube = collision.gameObject.GetComponent<CubesCollider>().myCube;
        }
    }
    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Cube")
        {
            Debug.Log(gameObject.name + "is connected to " + collision.gameObject.GetComponent<CubesCollider>().myCube.name);
            myCube.attachingCube = null;
        }
    }
 }
