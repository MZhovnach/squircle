﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public bool Direction;
    public GameObject Model;
    public float SPEED = 20.0f;
    public Rigidbody _rigidBody;
    protected bool Move;

    protected bool _directionRight;

    protected virtual void Start()
    {
        _directionRight = true;
        _rigidBody = GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {

    }

    protected virtual void OnCollisionEnter(Collision collision)
    {

    }
    protected virtual void OnCollisionExit(Collision collision)
    {

    }
    protected virtual void OnTriggerEnter(Collider collider)
    {

    }
    protected virtual void OnTriggerExit(Collider collider)
    {

    }

    protected void SetModelFlip(bool flip)
    {
        //Vector3 currentEuler = Model.transform.eulerAngles;
        //currentEuler.z = flip ? 0.0f : 180.0f;
        Model.transform.Rotate(new Vector3(0, 180, 0));
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        // Debug-draw all contact points and normals
        foreach (ContactPoint contact in collisionInfo.contacts)
        {
            //Debug.DrawRay(contact.point, contact.normal, Color.white);
            Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
            Vector3 currentEuler = transform.eulerAngles;
            currentEuler.z = rotation.eulerAngles.z;
            transform.eulerAngles = currentEuler;
        }
    }
}
