﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public Cube attachingCube = null;
    public Transform startPosition;
    public Player isPlayer = null;
    public List<Enemy> localEnemies = new List<Enemy>();
    private Gate leftGate;
    private Gate rightGate;
    public Vector3 startingpoint;
    public Collider cubesCollider;
    
    public Gate LeftGate
    {
        get
        {
            if (leftGate == null)
            {
                leftGate = transform.Find("LeftGate").GetComponent<Gate>();
            }
            return leftGate;
        }
        set
        {
            leftGate = value;
        }
    }

    public Gate RightGate
    {
        get
        {
            if (rightGate == null)
            {
                rightGate = transform.Find("RightGate").GetComponent<Gate>();
            }
            return rightGate;
        }
        set
        {
            rightGate = value;
        }
    }

    private void Start()
    {

    }

    public void RotateCube()
    {
        if (!LeanTween.isTweening(gameObject))
        {
            LeanTween.rotateAround(gameObject, Vector3.forward, 90.0f, 0.5f)
                     .setEase(LeanTweenType.easeInOutSine);
        }
        //transform.Rotate(Vector3.forward, 90.0f);
    }

}