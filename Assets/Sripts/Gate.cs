﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

    public Cube myCube;

    // Setting Enemy to another cube
    public void TransitionToAnotherCube(Enemy enemy)
    {
        Cube attachingCube = myCube.attachingCube;
        myCube.localEnemies.Remove(enemy);
        attachingCube.localEnemies.Add(enemy);
        enemy.transform.parent = attachingCube.transform;
        enemy.transform.position = attachingCube.startPosition.position;
        enemy.Direction = true;
    }
    public void TransitionToAnotherCube(Player player)
    {
        Cube attachingCube = myCube.attachingCube;
        player.transform.parent = attachingCube.transform;
        attachingCube.isPlayer = player;
        player.transform.position = attachingCube.startPosition.position;
        player.Direction = true;
    }
    //void OnTriggerEnter(Collider collision)
    //{
    //    if (collision.gameObject.tag == "Cube")
    //    {
    //        Debug.Log(gameObject.name + "is connected to " + collision.gameObject.GetComponent<Gate>().myCube.name);
    //        myCube.attachingCube = collision.gameObject.GetComponent<Gate>().myCube;
    //    }
    //}
    //void OnTriggerExit(Collider collision)
    //{
    //    if (collision.gameObject.tag == "Cube")
    //    {
    //        Debug.Log(gameObject.name + "is connected to " + collision.gameObject.GetComponent<Gate>().myCube.name);
    //        myCube.attachingCube = collision.gameObject.GetComponent<Gate>().myCube;
    //    }
    //}

}
