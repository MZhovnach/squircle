﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Movement
{
    public float velocityClampingValue;
    private bool transited = false; // transitions crunch #1
    private Collider previousCollider; // transition crunch #2
    // Update is called once per frame
    private void FixedUpdate()
    {
        _rigidBody.AddForce(transform.up * -1);
        _rigidBody.velocity = Vector3.ClampMagnitude(_rigidBody.velocity, velocityClampingValue);
    }

    void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            _rigidBody.AddForce(transform.right * SPEED);
            if (!_directionRight)
            {
                _directionRight = true;
                SetModelFlip(_directionRight);
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _rigidBody.AddForce(-1.0f * transform.right * SPEED);
            if (_directionRight)
            {
                _directionRight = false;
                SetModelFlip(_directionRight);
            }
        }
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.LogError("Game OVER");
        }

    }

    protected override void OnTriggerEnter(Collider collider)
    {
     
        if (!transited && collider.gameObject.tag == "Gate")
        {
            if (collider.gameObject.GetComponent<Gate>().myCube.attachingCube != null)
            {
                collider.gameObject.GetComponent<Gate>().TransitionToAnotherCube(this);
                _rigidBody.velocity = Vector3.zero;
                Debug.Log("Player's Gate Passing");
                previousCollider = collider;
                transited = true;
            }
        }
   
    }
    protected override void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Gate" && collider != previousCollider)
        {
            transited = false;
        }
    }
}
