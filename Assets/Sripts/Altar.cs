﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : MonoBehaviour
{

    public GameObject Fire;
    public GameObject FirePlace;
    private int _snailsCount = 4;
    public UIController uiController;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Invoke("DoFire", 1.0f);
            collision.gameObject.GetComponent<Enemy>().Kill();
            Destroy(collision.gameObject, 1.5f);
            _snailsCount--;
            if (_snailsCount <= 0)
            {
                Invoke("OnWinGame", 4.0f);
            }
        }
    }

    private void DoFire()
    {
        GameObject fireEffect = Instantiate(Fire, FirePlace.transform, false);
        Destroy(fireEffect, 3.0f);
    }

    private void OnWinGame()
    {
        uiController.ShowWinNotification();
    }
}