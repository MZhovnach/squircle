﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    // Use this for initialization
    public GameObject winNotification;
    public Vector3 winNotificationPosition;

    public GameObject looseNotification;
    public Vector3 looseNotificationPosition;

    void Start () {
        winNotificationPosition = winNotification.transform.position;
        looseNotificationPosition = looseNotification.transform.position;
    }

    public void ShowLooseNotification()
    {
      
        looseNotification.SetActive(true);
        //LeanTween.moveLocal(looseNotification, looseNotificationPosition + new Vector3(0, Screen.height / 2, 0), 2f)
        //    .setEase(LeanTweenType.easeOutCubic);

    }
    public void ShowWinNotification()
    {
        winNotification.SetActive(true);
        //LeanTween.moveLocal(winNotification, winNotificationPosition + new Vector3(0, Screen.height / 2, 0), 2f)
        //    .setEase(LeanTweenType.easeOutCubic);
    }

    public void OnRestartGamePressed()
    {
        SceneManager.LoadScene(0);
    }

}
