﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Movement
{
    private bool transited = false; // transitions crunch #1
    private Collider previousCollider; // transition crunch #2

    // Use this for initialization
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        Move = true;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (Move)
        {
            _rigidBody.AddForce(transform.right * SPEED);
        }
    }

    public void Kill()
    {
        Invoke("KillDelay", 1f);
    }

    public void KillDelay()
    {
        _rigidBody.velocity = Vector3.zero;
    }
    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.LogError("Game OVER");
        }

    }

    protected override void OnTriggerEnter(Collider collider)
    {

        if (!transited && collider.gameObject.tag == "Gate")
        {
            if (collider.gameObject.GetComponent<Gate>().myCube.attachingCube != null)
            {
                collider.gameObject.GetComponent<Gate>().TransitionToAnotherCube(this);
                _rigidBody.velocity = Vector3.zero;
                Debug.Log("Enemy Gate Passing");
                previousCollider = collider;
                transited = true;
            }
        }

    }
    protected override void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Gate" && collider != previousCollider)
        {
            transited = false;
        }
    }
}
